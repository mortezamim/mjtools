package ir.mj_dev.util;

public class NumberToWordsConverter
{
    final private static String strAnd = " و ";
    //    final private static String[] units = {"Zero", "One", "Two", "Three", "Four",
    //            "Five", "Six", "Seven", "Eight", "Nine", "Ten",
    //            "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen",
    //            "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
    //    final private static String[] tens = {"", "", "Twenty", "Thirty", "Forty", "Fifty",
    //            "Sixty", "Seventy", "Eighty", "Ninety"};
    final private static String[] units = {"صفر", "یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه",
            "ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده"};
    final private static String[] dahgan = {"", "", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود"};
    final private static String[] sadghan = {"", "یکصد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد"};
    final private static String steps[] = {"هزار", "میلیون", "میلیارد", "تریلیون", "کادریلیون", "کوینتریلیون",
            "سکستریلیون", "سپتریلیون", "اکتریلیون", "نونیلیون", "دسیلیون"};
    private static String strDigit[][] = {
            {"ده", "یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه", "صفر"},
            {"", "ده", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود"},
            {"", "یکصد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد"},
            {"ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده"},
            {"", " هزار", " میلیون", " میلیارد"}
    };


    //    billion trillion

    public static String convert(Integer i)
    {
        //
        if (i < 20) return units[i];
        if (i < 100) return dahgan[i / 10] + ((i % 10 > 0) ? strAnd + convert(i % 10) : "");
        if (i < 1000)
            return sadghan[i / 100] + ((i % 100 > 0) ? strAnd + convert(i % 100) : "");
        if (i < 1000000)
            return convert(i / 1000) + stepsString(steps[0]) + ((i % 1000 > 0) ? strAnd + convert(i % 1000) : "");
        if (i < 1000000000)
            return convert(i / 1000000) + stepsString(steps[1]) + ((i % 1000000 > 0) ? strAnd + convert(i % 1000000) : "");
        return convert(i / 1000000000) + stepsString(steps[2]) + ((i % 1000000000 > 0) ? strAnd + convert(i % 1000000000) : "");
    }


    private static String stepsString(String step)
    {
        return " " + step + " ";
    }


}

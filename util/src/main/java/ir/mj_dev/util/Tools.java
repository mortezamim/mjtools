package ir.mj_dev.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by MiM on 2/14/2018.
 */

public class Tools
{

    public static String humanReadableByteCount(long bytes, boolean si)
    {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    @SuppressLint("NewApi")
    public static int getSoftButtonsBarHeight(Activity activity)
    {
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
        {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            activity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }

    public static int getScreenWidth(Activity activity)
    {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public static int getScreenHeight(Activity activity)
    {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public static String convertENGtoPER(String value)
    {
        value = value.replace('0', '۰');
        value = value.replace('1', '۱');
        value = value.replace('2', '۲');
        value = value.replace('3', '۳');
        value = value.replace('4', '۴');
        value = value.replace('5', '۵');
        value = value.replace('6', '۶');
        value = value.replace('7', '۷');
        value = value.replace('8', '۸');
        value = value.replace('9', '۹');

        return value;
    }

    public static String convertPERtoENG(String value)
    {
        value = value.replace('۰', '0');
        value = value.replace('۱', '1');
        value = value.replace('۲', '2');
        value = value.replace('۳', '3');
        value = value.replace('۴', '4');
        value = value.replace('۴', '5');
        value = value.replace('۶', '6');
        value = value.replace('۷', '7');
        value = value.replace('۸', '8');
        value = value.replace('۹', '9');

        return value;
    }

    public static int convertDpToPx(Activity activity, int dp)
    {
        return Math.round(dp * (activity.getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));

    }

    public static int convertPxToDp(int px)
    {
        return Math.round(px / (Resources.getSystem().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int getActionBarSize(Activity activity)
    {
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());

        if (actionBarHeight == 0 && activity.getTheme().resolveAttribute(R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }

    public static boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /**
     * forceLayoutFullScreen()
     * MUST USE BEFORE  " SetContentView "
     * force layout to be fullscreen
     */
    public static Activity forceLayoutFullScreen(Activity activity)
    {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return activity;
    }

    public static int getStatusBarHeight(Activity activity)
    {
        int result = 0;
        int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0)
        {
            result = activity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
